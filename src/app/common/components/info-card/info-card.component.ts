import {Component, Input, EventEmitter, OnInit, Output, OnChanges} from '@angular/core';

@Component({
  selector: 'app-info-card',
  templateUrl: './info-card.component.html',
  styleUrls: ['./info-card.component.scss']
})
export class InfoCardComponent implements OnInit, OnChanges {

  @Input() data: any;
  @Output() clicked: EventEmitter<any> = new EventEmitter();
  constructor() { }

  ngOnInit() {
    console.log(this.data);
  }
  ngOnChanges() {
    console.log(this.data);
  }
  start() {
    this.clicked.emit(this.data);
  }

}
