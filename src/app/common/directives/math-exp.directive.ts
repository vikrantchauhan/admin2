import {Directive, ElementRef, ViewContainerRef} from '@angular/core';
import katex from 'katex';

@Directive({
  selector: '[appMathExp]'
})
export class MathExpDirective {

  constructor(Element: ElementRef, view: ViewContainerRef) {
    console.log(view.element.nativeElement as HTMLElement);
    setTimeout(() => {
      katex.render(Element.nativeElement.innerText, Element.nativeElement, {
        throwOnError: true
      });
    }, 3000);
    MathJax.Hub.Queue(['Typeset', MathJax.Hub, view.element.nativeElement]);
   // Element.nativeElement.innerText =  (view.element.nativeElement as HTMLElement).innerHTML;
  }

}
