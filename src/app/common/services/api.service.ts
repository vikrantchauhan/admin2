import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {retry, catchError, map} from 'rxjs/operators';
import {AppSettings} from '../../config';

const SERVER_URL = AppSettings.apiUrl;
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private url = '';
  private params = {};
  public responseCache = new Map();

  constructor(
    private http: HttpClient
  ) {
  }

  setUrl(url) {
    this.url = url;
  }

  getUrl() {
    return SERVER_URL + this.url;
  }

  setParams(params) {
    this.params = params;
  }

  getParams() {
    return this.params;
  }

  getHeaders() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }
    return httpOptions;
  }

  createStudent() {
    return this.http.post(this.getUrl(), this.getParams(), this.getHeaders())
      .pipe(
        catchError(this.handleError)
      );
  }

  // Error handling
  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
// Get client-side error
      errorMessage = error.error.message;
    } else {
// Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    return throwError(errorMessage);
  }


  public executePut() {
    return new Promise((resolve, reject) => {
      return this.http
        .put(this.getUrl(), this.getParams(), this.getHeaders())
        .pipe(
          map((res: Response) => {
            if (!res) {
              throw new Error('Value expected!');
            }
            return res.json();
          })
        )
        .subscribe(
          (res: any) => {
            if (res.data) {
              resolve(res.data);
            }
            resolve(res);
          },
          err => reject(err)
        );
    });
  }

  public executePost() {
    return new Promise((resolve, reject) => {

      return this.http
        .post(this.getUrl(), this.getParams(), this.getHeaders())
        .pipe(
          map((res: Response) => {
            if (!res) {
              throw new Error('Value expected!');
            }
            return res;
          })
        )
        .subscribe(
          (res: any) => {
            resolve(res);
          },
          err => reject(err)
        );
    });
  }

  public getContent(): Observable<any> {
    const beersFromCache = this.responseCache.get(this.getUrl());
    if (beersFromCache) {
      return of(beersFromCache);
    }
    const response = this.http.get<any>(this.getUrl());
    response.subscribe(beers => this.responseCache.set(this.getUrl(), beers));
    return response;
  }

  public executeGet(): Promise<any> {
    return new Promise((resolve, reject) => {
        return this.http
          .get(this.getUrl(), this.getHeaders())
          .pipe(
            map((res: Response) => {
              if (!res) {
                throw new Error('Value expected!');
              }
              return res;
            })
          )
          .subscribe(
            (res: any) => {
              if (res) {
                resolve(res);
              }
              resolve(res);
            },
            err => reject(err)
          );
    });
  }
}
