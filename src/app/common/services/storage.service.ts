import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }
  setdata(key, data) {
    sessionStorage.setItem(key, JSON.stringify(data));
  }
  getdata(key) {
    return JSON.parse(sessionStorage.getItem(key));
  }
  removedata(key) {
    sessionStorage.removeItem(key);
  }
  clearAll() {
    sessionStorage.clear();
  }
}
