import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import {ApiService} from '../../common/services/api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {StorageService} from '../../common/services/storage.service';

@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.scss']
})
export class TopicComponent implements OnInit {
  show = true;
  id;
  contentList: any;
  private isMobile: boolean;
  constructor(private location: Location, private api: ApiService, private route: ActivatedRoute,
              public router: Router, private store: StorageService) { }

  ngOnInit() {
    if ( window.innerWidth < 700) {
      this.isMobile = true;
    }
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    })
    this.getContent(this.id);
  }
  getContent(id) {
    this.api.setUrl('content/' + id);
    this.api.executeGet().then((data: any) => {
      if (data.success) {
        this.contentList = data.data.notes;
        this.router.navigate(['study', this.id, this.contentList[0].id]);
      } else {
      }
    });
  }
  toggleMobile() {
    if (this.isMobile) {
      this.show = ! this.show;
    }
  }
  backLink() {
    this.router.navigate(['my/study/chapter', this.store.getdata('chapter')._id]);
  }

}
