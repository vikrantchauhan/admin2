import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopicComponent } from './topic/topic.component';
import {StudyRoutingModule} from './study-routing.module';
import { ContentComponent } from './content/content.component';
import {SafeHtmlPipe} from '../common/pipes/safeHtml.pipe';
import {KatexModule} from 'ng-katex';
import {MathExpDirective} from '../common/directives/math-exp.directive';
import {KatexDirective} from 'ng-katex/src/ng-katex.directive';
@NgModule({
  declarations: [TopicComponent, ContentComponent, SafeHtmlPipe, KatexDirective, MathExpDirective],
  imports: [
    CommonModule,
    StudyRoutingModule,
    KatexModule
  ],
  providers: []
})
export class StudyModule { }
