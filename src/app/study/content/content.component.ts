import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import {ApiService} from '../../common/services/api.service';
import {ActivatedRoute, Router} from '@angular/router';
import {StorageService} from '../../common/services/storage.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent implements OnInit {
  id;
  content: any;
  equation = '\\ne';
  loads = false;
  contentData;
  paragraph: string = `
    You can write text, that contains expressions like this: $x ^ 2 + 5$ inside them. As you probably know.
    You also can write expressions in display mode as follows: $$\\sum_{i=1}^n(x_i^2 - \\overline{x}^2)$$.
    In first case you will need to use \\$expression\\$ and in the second one \\$\\$expression\\$\\$.
    To scape the \\$ symbol it's mandatory to write as follows: \\\\$
  `;
  constructor(private location: Location, private api: ApiService, private route: ActivatedRoute,
              public router: Router, private store: StorageService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.getContent(this.id);
    });
  }
  getContent(id) {
    this.api.setUrl('note/' + id);
    this.api.getContent().subscribe((res) => {
      this.content = res.data;
      this.contentData = this.content.content.map((con) => {
        console.log(con);
        const cont = con.content;
        console.log(cont);
        con.content = cont.replace(/orb{code}/g, '\\');
        return con;
      });
      setTimeout(() => {
        this.loads = true;
      }, 2000)
      console.log(this.contentData);
    });
    // this.api.executeGet().then((data: any) => {
    //   if (data.success) {
    //     this.content = data.data;
    //   } else {
    //   }
    // });
  }
  backLink() {
    this.router.navigate(['study', '5d142c6e595fabeef90b98c6']);
  }

}
