import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TopicComponent} from './topic/topic.component';
import {ContentComponent} from './content/content.component';

const routes: Routes = [
  {
    path: ':id', component: TopicComponent,
    children: [
      { path: ':id', component: ContentComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudyRoutingModule { }
