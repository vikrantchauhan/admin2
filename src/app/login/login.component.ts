import { Component, OnInit } from '@angular/core';
import {Md5} from 'md5-typescript';
import {ApiService} from '../common/services/api.service';
import * as jwt_decode from 'jwt-decode';
import {Router} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';
import {StorageService} from '../common/services/storage.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username;
  password;
  exampleText: any;
  constructor(private api: ApiService, public router: Router, private store: StorageService, public toastr: ToastrManager) { }

  ngOnInit() {
  }
  checkForNull(type) {
    if (type === undefined || type === '' || type === null) {
      return false;
    } else {
      return true;
    }
  }
  getDecodedAccessToken(token: string): any {
    try {
      return jwt_decode(token);
    } catch (Error) {
      return null;
    }
  }
  login() {
    if (this.checkForNull(this.username) && this.checkForNull(this.password)) {
    const params = {
      email: this.username,
      password: Md5.init(this.password)
    };
    this.api.setUrl('teacher/login');
    this.api.setParams(params);
    this.api.executePost().then((data: any) => {
      console.log(data.success);
      if (data.success) {
       console.log(this.getDecodedAccessToken(data.token));
       this.store.setdata('user', this.getDecodedAccessToken(data.token));
       this.router.navigate(['my/study']);
      } else {
        this.toastr.errorToastr('Wrong Email/Password or not registered.', 'Error!');
      }
    });
  } else {
      this.toastr.errorToastr('All fields are mandatory', 'Error!');
    }
  }

}
