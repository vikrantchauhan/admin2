const apiBaseUrl = 'http://localhost:4000/api/';
console.log(apiBaseUrl);

export class AppSettings {
  public static apiUrl = apiBaseUrl;
  public static timeout = 10000;
  public static appVersion = '1.0.2.65';
}


