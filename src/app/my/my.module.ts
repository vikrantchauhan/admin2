import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyRoutingModule } from './my-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CommonComponent } from './common/common.component';
import { SubjectsComponent } from './subjects/subjects.component';
import { ChaptersComponent } from './chapters/chapters.component';
import { ContestsComponent } from './contests/contests.component';
import { FriendsComponent } from './friends/friends.component';
import {InfoCardComponent} from '../common/components/info-card/info-card.component';
import { SubjectComponent } from './subject/subject.component';
import {BreadcrumComponent} from '../common/components/breadcrum/breadcrum.component';
@NgModule({
  declarations: [
    DashboardComponent,
    CommonComponent,
    SubjectsComponent,
    ChaptersComponent,
    ContestsComponent,
    FriendsComponent,
    InfoCardComponent,
    SubjectComponent,
    BreadcrumComponent
  ],
  imports: [
    CommonModule,
    MyRoutingModule
  ]
})
export class MyModule { }
