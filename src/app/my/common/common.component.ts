import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {StorageService} from '../../common/services/storage.service';
import {ApiService} from '../../common/services/api.service';

@Component({
  selector: 'app-common',
  templateUrl: './common.component.html',
  styleUrls: ['./common.component.scss']
})
export class CommonComponent implements OnInit {
  private classId: string;

  constructor(public router: Router, private store: StorageService, private api: ApiService) { }
  user: any = {};
  isMobile = false;
  show = true;
  classData: any = {};
  ngOnInit() {
    if ( window.innerWidth < 700) {
      this.isMobile = true;
    }
    this.classId = this.store.getdata('user').classId;
    this.getClass();
  }
  getClass() {
    this.api.setUrl('class/' + this.classId);
    this.api.executeGet().then((data: any) => {
      console.log(data.success);
      if (data.success) {
        this.classData = data.data;
      } else {
      }
    });
  }
  toggleMobile() {
    if (this.isMobile) {
      this.show = ! this.show;
    }
  }
  logout() {
    this.store.clearAll();
    this.router.navigate(['login']);
  }
}
