import { Component, OnInit } from '@angular/core';
import {ApiService} from '../../common/services/api.service';
import {Router} from '@angular/router';
import {StorageService} from '../../common/services/storage.service';

@Component({
  selector: 'app-subjects',
  templateUrl: './subjects.component.html',
  styleUrls: ['./subjects.component.scss']
})
export class SubjectsComponent implements OnInit {

  subjectList: any = [];
  classId;
  constructor(private api: ApiService, public router: Router, private store: StorageService) { }

  ngOnInit() {
    this.classId = this.store.getdata('user').classId;
    this.getSubjects();
  }
  getSubjects() {
    this.api.setUrl('subject/all/' + this.classId);
    this.api.executeGet().then((data: any) => {
      console.log(data.success);
      if (data.success) {
        this.subjectList = data.data;
      } else {
      }
    });
  }

  goToChapterList(event) {
    console.log(event);
    this.store.setdata('subject', event);
    this.router.navigate(['my/study', event._id]);
  }

}
