import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CommonComponent} from './common/common.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {SubjectsComponent} from './subjects/subjects.component';
import {ContestsComponent} from './contests/contests.component';
import {FriendsComponent} from './friends/friends.component';
import {ChaptersComponent} from './chapters/chapters.component';
import {SubjectComponent} from './subject/subject.component';

const routes: Routes = [
  {
    path: '',
    component: CommonComponent,
    children: [
      { path: '', component: SubjectsComponent},
      { path: 'dashboard', component: DashboardComponent },
      { path: 'study',
        children: [
          { path: '', component: SubjectsComponent },
          { path: ':id', component: SubjectComponent },
          { path: 'chapter/:id', component: ChaptersComponent }
        ]
      },
      { path: 'contests', component: ContestsComponent },
      { path: 'chapter/:id', component: ChaptersComponent },
      { path: 'friends', component: FriendsComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyRoutingModule { }
