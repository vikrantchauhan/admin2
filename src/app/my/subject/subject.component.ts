import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {StorageService} from '../../common/services/storage.service';
import {ApiService} from '../../common/services/api.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.scss']
})
export class SubjectComponent implements OnInit {

  subjectdata: any;
  chapterList: any = [];
  id;
  constructor(private store: StorageService, private route: ActivatedRoute,
              private location: Location, private api: ApiService, public router: Router) { }

  ngOnInit() {
    this.subjectdata = this.store.getdata('subject');
    console.log(this.subjectdata);
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });
    this.getChapters(this.id);
  }
  getChapters(id) {
    this.api.setUrl('chapter/' + id);
    this.api.executeGet().then((data: any) => {
      console.log(data.success);
      if (data.success) {
        this.chapterList = data.data;
      } else {
      }
    });
  }
  backLink() {
    this.router.navigate(['my/study']);
  }
  goToChapterList(event) {
    console.log(event);
    this.store.setdata('chapter', event);
    this.router.navigate(['my/study/chapter', event._id]);
  }

}
