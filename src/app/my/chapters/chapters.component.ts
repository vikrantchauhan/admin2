import { Component, OnInit } from '@angular/core';
import {StorageService} from '../../common/services/storage.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Location} from '@angular/common';
import {ApiService} from '../../common/services/api.service';

@Component({
  selector: 'app-chapters',
  templateUrl: './chapters.component.html',
  styleUrls: ['./chapters.component.scss']
})
export class ChaptersComponent implements OnInit {
  subjectdata;
  chapterdata: any;
  topicList: any = [];
  id;
  constructor(private store: StorageService, private route: ActivatedRoute,
              private location: Location, private api: ApiService, public router: Router) { }

  ngOnInit() {
    this.chapterdata = this.store.getdata('chapter');
    console.log(this.chapterdata);
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.getChapters(this.id);
    });
  }
  getChapters(id) {
    this.api.setUrl('topic/' + id);
    this.api.executeGet().then((data: any) => {
      console.log(data.success);
      if (data.success) {
        this.topicList = data.data;
      } else {
      }
    });
  }
  backLink() {
    this.store.removedata('chapter');
    this.router.navigate(['my/study', this.store.getdata('subject')._id]);
  }
  goToChapterList(event) {
    console.log(event);
    this.store.setdata('topic', event);
    this.router.navigate(['study', event._id]);
  }

}
