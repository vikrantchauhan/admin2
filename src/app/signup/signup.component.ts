import { Component, OnInit } from '@angular/core';
import {SelectItem} from 'primeng/api';
import {ApiService} from '../common/services/api.service';
import {Md5} from 'md5-typescript';
import {Router} from '@angular/router';
import {ToastrManager} from 'ng6-toastr-notifications';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  constructor(private api: ApiService, public router: Router, public toastr: ToastrManager) {
    this.classList = [
      {label: 'Select Class', value: null},
      {label: 'Class 9', value: 0},
      {label: 'Class 10', value: 1},
      {label: 'Class 11', value: 2},
      {label: 'Class 12', value: 3}
    ];
  }
  username;
  password;
  classId = '';
  classList: SelectItem[];
  ngOnInit() {
  }
  checkForNull(type) {
    if (type === undefined || type === '' || type === null) {
      return false;
    } else {
      return true;
    }
  }
  register() {
    // @ts-ignore
    if (this.checkForNull(this.username) && this.checkForNull(this.password) && this.checkForNull(this.classId)) {
      const params = {
        email: this.username,
        password: Md5.init(this.password),
        classId: this.classId
      };
      this.api.setUrl('student/register');
      this.api.setParams(params);
      this.api.createStudent().subscribe((data: any) => {
        console.log(data.success);
        if (data.success) {
          this.toastr.successToastr('Please login to continue.', 'Success!');
        } else {
          this.toastr.errorToastr('Email already exist.Choose another email.', 'Error!');
        }

      });
    } else {
      this.toastr.errorToastr('All fields are mandatory', 'Error!');
    }
  }

}
